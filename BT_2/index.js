
const express = require('express')
const f = require('fs');
const bodyParser = require('body-parser')
const app = express()
const port = 3000
const morgan = require('morgan')
const crypto = require("crypto");
const handlebars = require("express-handlebars")
const hbs = handlebars.create({
    helpers: {
        formatTimestamp: function (timestamp) {
            return moment.unix(+timestamp).format("DD-MM-YYYY HH:mm:ss")
        }
    }
})
const moment = require('moment');
// string to be hashed

// secret or salt to be hashed with
const secret = "quyendepzai@050195####!!!!";
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}))
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.set('views', './views');
app.use(express.static('public'))
app.get('/', (req, res) => {
    res.render('login');
});

// parse application/json
app.use(bodyParser.json())
app.use(morgan(':method :url :status :res[content-length] - :response-time ms :http-version'))
app.get("/",function(req,res){
  
})
app.post('/product', (req, res) => {
  console.log(req.params)
  res.json(req.body)
})
app.put('/product', (req, res) => {
  console.log(req.params)
  res.json(req.body)
})
app.post('/register', (req, res) => {
  const path = './register.json'
  if (!f.existsSync(path)) {
    f.writeFileSync('./register.json', JSON.stringify([]))
  }
  const { username, password } = req.body
  if (username && username.trim() != "") {
    if (password.length === 6) {
      const sha256Hasher = crypto.createHmac("sha256", secret);
      const passwordHas = sha256Hasher.update(password).digest("hex");
      let lstUser = require("./register.json")
      if (lstUser && lstUser.length > 0) {
        let isExisted = false
        for (let i = 0; i < lstUser.length; i++) {
          const u = lstUser[i];
          if (username == u.username) {
            isExisted = true
            break
          }
        }
        if (!isExisted) {
          lstUser.push({
            username: username,
            password: passwordHas
          })
          f.writeFileSync('./register.json', JSON.stringify(lstUser))
          return res.json({status:"Success", message: "Dang ky thanh cong!"})
        } else {
          return res.json({status:"Existed", message: "Ten tai khoan da ton tai!"})
        }
      } else {
        let lstObject = []
        lstObject.push({
          username: username,
          password: passwordHas
        })
        f.writeFileSync('./register.json', JSON.stringify(lstObject))
        return res.json({status:"Success", message: "Dang ky thanh cong!"})
      }
    } else {
      return res.json({status:"Error", message: "Mat khau phai co 6 ky tu!"})
    }
  } else {
    return res.json({status:"Error", message: "Vui long nhap ten tai khoan!"})
  }
})
app.get('/user', function (req, res) {
  const user = require("./register.json")
  res.json(user)
})
app.post('/login', function (req, res) {
  const{username, password} = req.body
  if (!username) {
    return res.json({status:"Error", message: "Vui long nhap ten tai khoan!"})
  } else if (!password) {
    return res.json({status:"Error", message: "Vui long nhap ten mat khau!"})
  } else {
    const sha256Hasher = crypto.createHmac("sha256", secret);
    const passwordHas = sha256Hasher.update(password).digest("hex");
    console.log(passwordHas)
    const lstUser = require("./register.json")
    if (lstUser && lstUser.length > 0) {
      for (let i = 0; i < lstUser.length; i++) {
        const element = lstUser[i];
        if (element.username === username && element.password === passwordHas) {
         return res.render("home", { name: username })
        }
      }
    } else {
      return res.json({status:"Error", message: "Khong ton tai tai khoan tren he thong!"})
    }
    return res.json({status:"Error", message: "Ten tai khoan hoac mat khau khong chinh xac!"})
  }
  res.json(req.query)
})
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})